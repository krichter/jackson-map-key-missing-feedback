package richtercloud.jackson.map.key.missing.feedback;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author richter
 */
public class Entity1 {
    @JsonSerialize(using = ValueMapSerializer.class)
    @JsonDeserialize(using = ValueMapDeserializer.class)
    private Map<Entity2, String> valueMap = new HashMap<>();
    private Entity3 entity3;

    public Entity1() {
    }

    public Map<Entity2, String> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<Entity2, String> valueMap) {
        this.valueMap = valueMap;
    }

    public Entity3 getEntity3() {
        return entity3;
    }

    public void setEntity3(Entity3 entity3) {
        this.entity3 = entity3;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.valueMap);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entity1 other = (Entity1) obj;
        if (!Objects.equals(this.valueMap, other.valueMap)) {
            return false;
        }
        return true;
    }
}
