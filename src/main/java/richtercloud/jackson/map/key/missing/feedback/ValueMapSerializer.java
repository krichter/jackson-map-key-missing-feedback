package richtercloud.jackson.map.key.missing.feedback;

import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.Map;

/**
 *
 * @author richter
 */
public class ValueMapSerializer extends MapEntrySerializer<Entity2, String> {
    private static final long serialVersionUID = 1L;

    public ValueMapSerializer() {
        super(TypeFactory.defaultInstance().constructMapType(Map.class, Entity2.class, String.class));
    }
}
