package richtercloud.jackson.map.key.missing.feedback;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author richter
 */
public class Entity3 {
    private Map<Entity2, Boolean> otherValueMap = new HashMap<>();

    public Entity3() {
    }

    public Map<Entity2, Boolean> getOtherValueMap() {
        return otherValueMap;
    }

    public void setOtherValueMap(Map<Entity2, Boolean> otherValueMap) {
        this.otherValueMap = otherValueMap;
    }
}
