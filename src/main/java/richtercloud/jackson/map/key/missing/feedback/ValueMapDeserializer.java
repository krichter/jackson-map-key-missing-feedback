package richtercloud.jackson.map.key.missing.feedback;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author richter
 */
public class ValueMapDeserializer extends MapEntryDeserializer<Entity2, String> {
    private static final long serialVersionUID = 1L;

    public ValueMapDeserializer() {
        super(TypeFactory.defaultInstance().constructMapType(Map.class, Entity2.class, String.class));
    }

    @Override
    public Map<Entity2, String> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Map<Entity2, String> retValue = new HashMap<>();
        List<Entry<Entity2, String>> entries = p.readValueAs(new TypeReference<List<Entry<Entity2, String>>>() {});
        for(Entry<Entity2, String> entry : entries) {
            retValue.put(entry.getKey(),
                    entry.getValue());
        }
        return retValue;
    }

    private static class Entry<K,V> {
        private K key;
        private V value;

        Entry() {
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }
    }
}
