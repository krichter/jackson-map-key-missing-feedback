package richtercloud.jackson.map.key.missing.feedback;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author richter
 */
public abstract class MapEntrySerializer<K, V> extends StdSerializer<Map<K, V>> {
    private static final long serialVersionUID = 1L;

    public MapEntrySerializer(Class<Map<K, V>> t) {
        super(t);
    }

    public MapEntrySerializer(JavaType type) {
        super(type);
    }

    /**
     * Serializes a {@link java.util.Map} a list of its entries which are JSON
     * object consisting of key and value. This allows arbitrary objects to be
     * mapped as map keys as well as easy reusage of ID references. Inspired by
     * http://www.baeldung.com/jackson-map.
     *
     * Note that any serialization as object fails since field names have to be
     * strings (serializing the complex key to a raw string is far from
     * elegant). The fact the maps are effectively list in the serialization
     * output is a small price to pay.
     *
     * @param value
     * @param gen
     * @param serializers
     * @throws IOException
     */
    @Override
    public void serialize(Map<K, V> value,
            JsonGenerator gen,
            SerializerProvider serializers) throws IOException {
        gen.writeStartArray(value.size() //size
        );
            //specifying size should increase the code stability
        for(Entry<K,V> entry : value.entrySet()) {
            gen.writeStartObject(entry);
            if(gen.canWriteObjectId()) {
                gen.writeObjectId(entry.getKey());
            }else {
                gen.writeObjectField("key", entry.getKey());
                gen.writeObjectField("value", entry.getValue());
            }
                //@TODO: this does not reuse the already added OfferProperty and
                //by writing its idea instead of adding the output again
            gen.writeEndObject();
        }
        gen.writeEndArray();
    }
}
