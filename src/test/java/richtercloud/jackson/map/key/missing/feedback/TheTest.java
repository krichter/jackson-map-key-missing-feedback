package richtercloud.jackson.map.key.missing.feedback;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author richter
 */
public class TheTest {

    /**
     * Test of getValueMap method, of class Entity1.
     */
    @Test
    public void testJsonSerialization() throws JsonProcessingException, IOException {
        Entity1 entity1 = new Entity1();
        Entity2 entity2 = new Entity2(1l);
        entity1.getValueMap().put(entity2, "test");
        ObjectMapper objectMapper = new ObjectMapper();
        String serialized = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entity1);
        Entity1 deserialized = objectMapper.readValue(serialized,
                Entity1.class);
        assertEquals(entity1,
                deserialized);
    }
}
